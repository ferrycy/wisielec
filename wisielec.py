#!/usr/bin/env python3

import string
from time import sleep
from losowe_haslo import losuj_hasło

LITERY = string.ascii_uppercase + 'ĄĆĘŁŃÓŚŹŻ'
PODKREŚLNIK = '_'
ODSTĘP = '\n\n'
MAKS_PRÓB = 10


def koduj_hasło(hasło, wykorzystane_litery):
    '''Zwraca sformatowane hasło z podkreślnikami
    w miejscu niewykorzystanych liter.'''
    zakodowane_hasło = []

    for znak in hasło:
        if (znak not in LITERY) or (znak in wykorzystane_litery):
            kod = znak
        else:
            kod = PODKREŚLNIK

        zakodowane_hasło.append(kod)

    return ' '.join(zakodowane_hasło)


def pokaż_dostępne_litery(wykorzystane_litery):
    '''Łańcuch niewykorzystanych liter.'''
    return ' '.join(lit for lit in LITERY if lit not in wykorzystane_litery)


def szubienica(pozostało_liter,
               pozostało_prób,
               dostępne_litery,
               zakodowane_hasło):
    '''Wyświetla stan gry.'''
    print(ODSTĘP)
    print('Pozostało liter: {}. Pozostało prób {}'.format(pozostało_liter,
                                                          pozostało_prób))
    print('Dostępne litery:')
    print(dostępne_litery)
    print()
    print(zakodowane_hasło)
    print()
    print('Twoja litera: ', end='')


def ostrzeżenie(tekst, znaczek='#', sec=2):
    d = min(20, len(tekst))
    print()
    print(d * znaczek)
    print(tekst)
    print(d * znaczek)
    print()
    sleep(sec)


def wisielec():
    '''Gra w Wisielca na podstawie tytułu
    losowego hasła z Wikipedii.'''
    print('Szukam hasła, proszę czekać... :-)')
    hasło, adres_url = losuj_hasło()
    hasło = hasło.upper()
    liczba_błędów = 0
    pozostało_prób = MAKS_PRÓB
    wykorzystane_litery = ''
    dostępne_litery = pokaż_dostępne_litery(wykorzystane_litery)
    zakodowane_hasło = koduj_hasło(hasło, wykorzystane_litery)
    pozostało_liter = zakodowane_hasło.count(PODKREŚLNIK)

    while True:
        # Pokaż stan i pobierz literę.
        while True:
            szubienica(pozostało_liter,
                       pozostało_prób,
                       dostępne_litery,
                       zakodowane_hasło)
            litera = input().upper()

            if len(litera) != 1:
                ostrzeżenie('Możesz podać tylko jedną literę!')
                continue

            if litera not in LITERY:
                ostrzeżenie('Podaj LITERĘ!')
                continue

            if litera in wykorzystane_litery:
                ostrzeżenie('Ta litera już była!')
                continue

            break

        wykorzystane_litery += litera
        dostępne_litery = pokaż_dostępne_litery(wykorzystane_litery)

        if litera in hasło:
            zakodowane_hasło = koduj_hasło(hasło, wykorzystane_litery)
            pozostało_liter = zakodowane_hasło.count(PODKREŚLNIK)

            if pozostało_liter == 0:  # Wygrana.
                print(ODSTĘP)
                print(zakodowane_hasło)
                print(adres_url)
                print('Brawo! Wygrałeś!')
                print(ODSTĘP)
                return

        else:
            liczba_błędów += 1
            pozostało_prób -= 1

            if pozostało_prób == 0:  # Przegrana.
                zakodowane_hasło = koduj_hasło(hasło, LITERY)
                print(ODSTĘP)
                print(zakodowane_hasło)
                print(adres_url)
                print('Wisisz!')
                print(ODSTĘP)
                return


if __name__ == '__main__':
    wisielec()
